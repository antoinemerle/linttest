package com.deezer.tools.lint_rules;

import com.android.tools.lint.detector.api.ApiKt;
import com.android.tools.lint.detector.api.Issue;

import java.util.Arrays;
import java.util.List;



public final class IssueRegistry extends com.android.tools.lint.client.api.IssueRegistry {

    @Override
    public int getApi() {
        return ApiKt.CURRENT_API;
    }

    @Override
    public List<Issue> getIssues() {
        return Arrays.asList(SpekWithoutRunnerDetector.ISSUE__SPEK_WITHOUT_RUNNER);
    }
}