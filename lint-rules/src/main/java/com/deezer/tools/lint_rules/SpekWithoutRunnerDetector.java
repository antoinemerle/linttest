package com.deezer.tools.lint_rules;

import com.android.tools.lint.detector.api.Category;
import com.android.tools.lint.detector.api.Detector;
import com.android.tools.lint.detector.api.Implementation;
import com.android.tools.lint.detector.api.Issue;
import com.android.tools.lint.detector.api.JavaContext;
import com.android.tools.lint.detector.api.Scope;
import com.android.tools.lint.detector.api.Severity;

import org.jetbrains.uast.UAnnotation;
import org.jetbrains.uast.UClass;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;


public class SpekWithoutRunnerDetector extends Detector implements Detector.UastScanner {


    private static final EnumSet<Scope> SCOPE = EnumSet.of(Scope.ALL_JAVA_FILES, Scope.TEST_SOURCES);
    public static final Issue ISSUE__SPEK_WITHOUT_RUNNER = Issue.create(
            "SpekWithoutJunit4Runner",
            "If you use Spek, you have to use @RunWith annotation",
            "JUnit5 is not yet compatible with Android modules, meaning we have to force JUnit4 to be used.",
            Category.CORRECTNESS,
            8,
            Severity.ERROR,
            new Implementation(SpekWithoutRunnerDetector.class, SCOPE)
    );

    @Override
    public List<String> applicableSuperClasses() {
        return Arrays.asList("org.jetbrains.spek.api.Spek");
    }

    @Override
    public void visitClass(JavaContext context, UClass declaration) {
        // just log to check the Spek classes are visited
        System.out.println("trying to visit " + declaration.getQualifiedName());
        System.out.println("\tsuper=" + declaration.getUastSuperTypes());

        final UAnnotation annotation = declaration.findAnnotation("org.junit.runner.RunWith");
        if (annotation != null) return;
        // not found
        context.report(ISSUE__SPEK_WITHOUT_RUNNER, declaration,
                context.getLocation(declaration.getPsi()),
                "Missing @RunWith");
    }
}
