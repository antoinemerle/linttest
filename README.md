This project is here to demonstrate a bug using custom lint rules.

The module `lint-rules` contains a rule `SpekWithoutJunit4Runner`, which should visit every class extending `org.jetbrains.spek.api.Spek`.
Whenever one of these class do not have the annotation `@RunWith`, it should report an error.

The module `lint-test` contains 3 test classes that should be visited and reported as errors by the created lint rule.

It seems that these classes are never visited, may be because the Spek dependency is added as a `testImplementation` dependency.

Indeed, when changing 'testImplementation' into 'implementation', the Spek classes are visited.
Also, just adding `compileOnly "org.jetbrains.spek:spek-api:1.1.5"` makes it work.

